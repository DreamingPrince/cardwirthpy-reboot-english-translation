#!/usr/bin/env python
# -*- coding: utf-8 -*-

from . import base
from . import card
from . import background
from . import message
from . import scrollbar
from . import statusbar
from . import touchbutton
from . import transition
from . import animationcell
from . import bill


def main() -> None:
    pass


if __name__ == "__main__":
    main()
