Patchのライセンス
=================

各パッチは、パッチ先のライセンスに従います。
（Unified patch 形式を利用しており、パッチ先のソースコードの一部が
  含まれるため）
注記がない物はMURAMATSU Atsushiが作成しています。

* gettext-use-system-localedir.patch
  GNU Lesser General Public License

* glib-disable-giomodule.patch  
  GNU Lesser General Public License

* glib-fdopendir-check.patch  
  GNU Lesser General Public License
  
* libpng-nozlib.patch  
  libpng License

* libsdl-1.2.15-const-xdata32.patch  
  GNU Lesser General Public License
  このパッチは、
  [gentoo のレポジトリ](https://sources.gentoo.org/cgi-bin/viewvc.cgi/gentoo-x86/media-libs/libsdl/files/libsdl-1.2.15-const-xdata32.patch)
  から貰ってきています。

* libsdl-1.2.15-without-macframework.patch  
  GNU Lesser General Public License

* pygame-disable-joystick.patch  
  GNU Lesser General Public License

* pygame-without-cocoa-carbon.patch  
  GNU Lesser General Public License

* wxpython-clang.patch  
  wxWindows Library License

* wxpython-mac-gtk2.patch  
  wxWindows Library License
